# Drupal Settings symfony mailer with SMTP

Need to create a mailer transport with the ID `dsn`.
That will be overriden with the environment variables.

## Variables available
```
SMTP_USERNAME
SMTP_PASSWORD
SMTP_SERVER
```

if `SMTP_GLOBAL_SENDER_IS_USERNAME` is set then the global mail for the site (`$config['system.site']['mail']`)
will be overriden with the smtp username since it will be probably the only email address that can be used to send.
