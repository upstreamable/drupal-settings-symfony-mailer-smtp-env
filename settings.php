<?php

$config['symfony_mailer.mailer_transport.dsn']['configuration'] = [
  'dsn' => 'smtp://' . getenv('SMTP_USERNAME') . ':' . getenv('SMTP_PASSWORD') . '@' . getenv('SMTP_SERVER'),
];
$config['symfony_mailer.settings']['default_transport'] = 'dsn';

if (getenv('SMTP_GLOBAL_SENDER_IS_USERNAME') && getenv('SMTP_USERNAME')) {
  $config['system.site']['mail'] = getenv('SMTP_USERNAME');
}
